package ch.usi.si.codelounge.seminar.shapes.dsl.builders

import ch.usi.si.codelounge.seminar.shapes.model.shapes.Triangle

class TriangleBuilder : SingleShapeBuilder() {
    override fun build() = Triangle(size, color)
}
