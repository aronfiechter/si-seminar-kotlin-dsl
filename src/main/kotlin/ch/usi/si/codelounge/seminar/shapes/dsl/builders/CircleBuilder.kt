package ch.usi.si.codelounge.seminar.shapes.dsl.builders

import ch.usi.si.codelounge.seminar.shapes.model.shapes.Circle

class CircleBuilder : SingleShapeBuilder() {
    override fun build() = Circle(size, color)
}
