package ch.usi.si.codelounge.seminar.shapes.model.shapes

import org.jetbrains.skija.BlendMode
import org.jetbrains.skija.Canvas

abstract class Shape(val size: Int) : Entity() {
    // TODO: could be a template method? (maybe in SingleShape)
    abstract fun paintOn(canvas: Canvas, position: Point, blendMode: BlendMode? = null, alpha: Int? = 255): Unit
}
