package ch.usi.si.codelounge.seminar.shapes.model.shapes

import org.jetbrains.skija.BlendMode
import org.jetbrains.skija.Canvas

class Circle(size: Int, color: Color) : SingleShape(size, color) {
    override fun paintOn(canvas: Canvas, position: Point, blendMode: BlendMode?, alpha: Int?) {
        val (x, y) = position
        val radius = size / 2
        val paint = getPaint(blendMode, alpha)
        canvas.drawCircle((x + radius).toFloat(), (y + radius).toFloat(), radius.toFloat(), paint)
    }

    override fun toString(): String = "Circle(${size}, ${color})"
}
