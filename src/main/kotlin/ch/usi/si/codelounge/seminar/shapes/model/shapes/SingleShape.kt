package ch.usi.si.codelounge.seminar.shapes.model.shapes

import org.jetbrains.skija.BlendMode
import org.jetbrains.skija.Paint


abstract class SingleShape(size: Int, val color: Color): Shape(size) {
    fun getPaint(blendMode: BlendMode?, alpha: Int?): Paint {
        return color.paint
            .apply { if (alpha != null) setAlpha(alpha) }
            .apply { if (blendMode != null) setBlendMode(blendMode) }
    }
}
