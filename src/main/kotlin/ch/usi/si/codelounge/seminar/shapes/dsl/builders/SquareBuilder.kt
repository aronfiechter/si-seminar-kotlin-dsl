package ch.usi.si.codelounge.seminar.shapes.dsl.builders

import ch.usi.si.codelounge.seminar.shapes.model.shapes.Square

class SquareBuilder : SingleShapeBuilder() {
    override fun build() = Square(size, color)
}
