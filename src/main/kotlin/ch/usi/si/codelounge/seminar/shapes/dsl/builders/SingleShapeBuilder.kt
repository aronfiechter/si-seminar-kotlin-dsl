package ch.usi.si.codelounge.seminar.shapes.dsl.builders

import ch.usi.si.codelounge.seminar.shapes.dsl.ShapesDsl
import ch.usi.si.codelounge.seminar.shapes.model.shapes.Color.BLACK
import ch.usi.si.codelounge.seminar.shapes.model.shapes.SingleShape

@ShapesDsl
abstract class SingleShapeBuilder {
    var size = 0
    var color = BLACK

    abstract fun build(): SingleShape
}
