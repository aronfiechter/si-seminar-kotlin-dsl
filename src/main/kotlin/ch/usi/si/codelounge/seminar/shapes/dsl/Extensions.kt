package ch.usi.si.codelounge.seminar.shapes.dsl

import ch.usi.si.codelounge.seminar.shapes.dsl.builders.CircleBuilder
import ch.usi.si.codelounge.seminar.shapes.dsl.builders.SquareBuilder
import ch.usi.si.codelounge.seminar.shapes.dsl.builders.TriangleBuilder
import ch.usi.si.codelounge.seminar.shapes.model.picture.Picture
import ch.usi.si.codelounge.seminar.shapes.model.shapes.*

@DslMarker
@Target(AnnotationTarget.CLASS, AnnotationTarget.TYPE)
annotation class ShapesDsl

fun picture(init: (@ShapesDsl Picture).() -> Unit): Picture {
    val picture = Picture()
    picture.init()
    return picture
}

fun Picture.circle(init: CircleBuilder.() -> Unit): Circle {
    val builder = CircleBuilder()
    builder.init()
    val circle = builder.build()
    this.addShape(circle)
    return circle
}

fun Picture.square(init: SquareBuilder.() -> Unit): Square {
    val builder = SquareBuilder()
    builder.init()
    val square = builder.build()
    this.addShape(square)
    return square
}

fun Picture.triangle(init: TriangleBuilder.() -> Unit): Triangle {
    val builder = TriangleBuilder()
    builder.init()
    val triangle = builder.build()
    this.addShape(triangle)
    return triangle
}

fun Picture.lineBreak(): Unit {
    this.addLineBreak()
}

infix fun Shape.union(other: Shape): ComposedShape = ComposedShape(this, other, Operation.UNION)
infix fun Shape.difference(other: Shape): ComposedShape = ComposedShape(this, other, Operation.DIFFERENCE)
operator fun Shape.plus(other: Shape) = this union other
operator fun Shape.minus(other: Shape) = this difference other

fun Picture.composed(init: () -> ComposedShape): ComposedShape {
    val composedShape = init()
    this.addShape(composedShape)
    return composedShape
}
