package ch.usi.si.codelounge.seminar.shapes.model.picture

import ch.usi.si.codelounge.seminar.shapes.model.core.LineBreak
import ch.usi.si.codelounge.seminar.shapes.model.shapes.Shape
import org.jetbrains.skija.Canvas
import org.jetbrains.skija.Surface

object Painter {

    fun paintAndSave(picture: Picture, fileName: String): Unit {
        val bytes = getBytes(picture)
        if (bytes != null && bytes.isNotEmpty()) {
            java.nio.file.Files.write(java.nio.file.Path.of(fileName), bytes);
        } else {
            throw IllegalArgumentException("Picture contains no data!")
        }
    }

    fun getRows(picture: Picture): List<Row> {
        val initial = listOf(mutableListOf<Shape>())
        val listsOfShapes = picture.getEntities().fold(initial) { lines, shape ->
            val previousLines = lines.dropLast(1)
            val line = lines.last()
            when (shape) {
                is LineBreak -> lines + listOf(mutableListOf())
                else -> previousLines + listOf(line.apply { add(shape as Shape) })
            }
        }
        val ys = listsOfShapes.runningFold(0) { y, shapes -> y + shapes.maxOf { it.size } }
        return listsOfShapes.zip(ys).map { (shapes, y) -> Row(shapes, y) }
    }

    private fun getBytes(picture: Picture): ByteArray? {
        val lines = getRows(picture)
        val width = lines.maxOf { it.width }
        val height = lines.sumOf { it.height }
        val surface = Surface.makeRasterN32Premul(width, height)
        val canvas = surface.getCanvas()
        lines.forEach { it.paintOn(canvas) }
        return surface.makeImageSnapshot().encodeToData()?.getBytes()
    }
}

class Row(val shapes: List<Shape>, val y: Int) {
    val width: Int by lazy { shapes.sumOf { it.size } }
    val height: Int by lazy { shapes.maxOf { it.size } }

    fun paintOn(canvas: Canvas): Unit {
        val xs = shapes.runningFold(0) { x, shape -> x + shape.size }
        shapes.zip(xs).forEach { (shape, x) ->
            shape.paintOn(canvas, Pair(x, y))
        }
    }
}