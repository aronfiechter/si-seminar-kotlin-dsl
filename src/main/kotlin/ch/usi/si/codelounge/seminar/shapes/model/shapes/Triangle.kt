package ch.usi.si.codelounge.seminar.shapes.model.shapes

import org.jetbrains.skija.BlendMode
import org.jetbrains.skija.Canvas
import org.jetbrains.skija.Point as SPoint

class Triangle(size: Int, color: Color) : SingleShape(size, color) {
    override fun paintOn(canvas: Canvas, position: Point, blendMode: BlendMode?, alpha: Int?) {
        val (x, y) = position
        val paint = getPaint(blendMode, alpha)
        canvas.drawTriangles(
            arrayOf(
                SPoint((x + size / 2).toFloat(), y.toFloat()),
                SPoint(x.toFloat(), (y + size).toFloat()),
                SPoint((x + size).toFloat(), (y + size).toFloat())
            ), null, paint
        )
    }

    override fun toString(): String = "Triangle(${size}, ${color})"
}
