package ch.usi.si.codelounge.seminar.shapes.model.shapes

import org.jetbrains.skija.Paint

open class Entity

typealias Point = Pair<Int, Int>

enum class Color(val paint: Paint) {
    RED(Paint().setColor(0xFFFF0000.toInt()).setAntiAlias(false)),
    GREEN(Paint().setColor(0xFF00FF00.toInt()).setAntiAlias(false)),
    BLUE(Paint().setColor(0xFF0000FF.toInt()).setAntiAlias(false)),
    BLACK(Paint().setColor(0xFF000000.toInt()).setAntiAlias(false))
}
