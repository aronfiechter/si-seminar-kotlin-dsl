package ch.usi.si.codelounge.seminar.shapes.model.shapes

import org.jetbrains.skija.BlendMode
import org.jetbrains.skija.Canvas
import org.jetbrains.skija.Rect

class Square(size: Int, color: Color) : SingleShape(size, color) {
    override fun paintOn(canvas: Canvas, position: Point, blendMode: BlendMode?, alpha: Int?) {
        val (x, y) = position
        val paint = getPaint(blendMode, alpha)
        val rect = Rect(x.toFloat(), y.toFloat(), (x + size).toFloat(), (y + size).toFloat())
        canvas.drawRect(rect, paint)
    }

    override fun toString(): String = "Square(${size}, ${color})"
}
