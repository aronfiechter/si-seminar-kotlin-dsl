package ch.usi.si.codelounge.seminar.shapes.model.shapes

import ch.usi.si.codelounge.seminar.shapes.model.shapes.Operation.DIFFERENCE
import ch.usi.si.codelounge.seminar.shapes.model.shapes.Operation.UNION
import org.jetbrains.skija.BlendMode
import org.jetbrains.skija.Canvas

enum class Operation {
    UNION, DIFFERENCE
}

class ComposedShape(
    private val first: Shape,
    private val second: Shape,
    private val operation: Operation
) : Shape(maxOf(first.size, second.size)) {

    private fun getBlendMode(): BlendMode = when (operation) {
        UNION -> BlendMode.DIFFERENCE
        DIFFERENCE -> BlendMode.SRC_OUT
    }

    override fun paintOn(canvas: Canvas, position: Point, blendMode: BlendMode?, alpha: Int?) {
        val nextBlendMode = getBlendMode()
        second.paintOn(canvas, position, blendMode, alpha)
        first.paintOn(canvas, position, nextBlendMode, alpha)
    }

}