package ch.usi.si.codelounge.seminar.shapes.model.picture

import ch.usi.si.codelounge.seminar.shapes.model.shapes.Entity
import ch.usi.si.codelounge.seminar.shapes.model.core.LineBreak
import ch.usi.si.codelounge.seminar.shapes.model.shapes.Shape

class Picture {

    private var entities: MutableList<Entity> = mutableListOf()

    fun getEntities(): List<Entity> = entities

    fun addShape(shape: Shape): Picture {
        return apply { entities.add(shape) }
    }

    fun addLineBreak(): Picture {
        return apply { entities.add(LineBreak) }
    }

    fun paint(fileName: String = "picture.png"): Unit {
        Painter.paintAndSave(this, fileName)
    }
}
