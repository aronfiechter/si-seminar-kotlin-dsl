package ch.usi.si.codelounge.seminar.shapes

import ch.usi.si.codelounge.seminar.shapes.dsl.*
import ch.usi.si.codelounge.seminar.shapes.model.shapes.Color.*

fun main() {
    picture {
        val greenCircle = circle {
            size = 100
            color = GREEN
        }
        val redSquare = square {
            size = 100
            color = RED
        }
        val blueSquare = square {
            size = 100
            color = BLUE
        }
        val redTriangle = triangle {
            size = 100
            color = RED
        }

        lineBreak()

        listOf(60, 70, 80, 90, 100).forEach {
            circle { size = it }
        }

        lineBreak()

        composed { redSquare + blueSquare }
        composed { blueSquare + greenCircle }
        composed { redTriangle - greenCircle }
        composed { redTriangle + blueSquare - greenCircle }
    }.paint()
}
