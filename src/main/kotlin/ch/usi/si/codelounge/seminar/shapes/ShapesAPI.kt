package ch.usi.si.codelounge.seminar.shapes

import ch.usi.si.codelounge.seminar.shapes.model.picture.Picture
import ch.usi.si.codelounge.seminar.shapes.model.shapes.Circle
import ch.usi.si.codelounge.seminar.shapes.model.shapes.Color.*
import ch.usi.si.codelounge.seminar.shapes.model.shapes.ComposedShape
import ch.usi.si.codelounge.seminar.shapes.model.shapes.Operation.DIFFERENCE
import ch.usi.si.codelounge.seminar.shapes.model.shapes.Operation.UNION
import ch.usi.si.codelounge.seminar.shapes.model.shapes.Square
import ch.usi.si.codelounge.seminar.shapes.model.shapes.Triangle

fun main() {
    val picture = Picture()

    val greenCircle = Circle(100, GREEN)
    val redSquare = Square(100, RED)
    val blueSquare = Square(100, BLUE)
    val redTriangle = Triangle(100, RED)
    picture.addShape(greenCircle)
    picture.addShape(redSquare)
    picture.addShape(blueSquare)
    picture.addShape(redTriangle)
    picture.addLineBreak()

    listOf(60, 70, 80, 90, 100).forEach {
        picture.addShape(Circle(it, BLACK))
    }
    picture.addLineBreak()

    picture.addShape(
        ComposedShape(redSquare, blueSquare, UNION)
    )
    picture.addShape(
        ComposedShape(blueSquare, greenCircle, UNION)
    )
    picture.addShape(
        ComposedShape(redTriangle, greenCircle, DIFFERENCE)
    )
    picture.addShape(
        ComposedShape(
            ComposedShape(redTriangle, blueSquare, UNION),
            greenCircle,
            DIFFERENCE
        )
    )

    picture.paint("picture-api.png")
}