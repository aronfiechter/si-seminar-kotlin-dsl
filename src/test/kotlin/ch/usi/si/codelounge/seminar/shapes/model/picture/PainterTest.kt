package ch.usi.si.codelounge.seminar.shapes.model.picture

import ch.usi.si.codelounge.seminar.shapes.model.shapes.Circle
import ch.usi.si.codelounge.seminar.shapes.model.shapes.Color
import ch.usi.si.codelounge.seminar.shapes.model.shapes.Square
import ch.usi.si.codelounge.seminar.shapes.model.shapes.Triangle
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class PainterTest {

    private lateinit var testPicture: Picture
    private val testSquare = Square(75, Color.BLUE)
    private val testCircle = Circle(125, Color.GREEN)
    private val testTriangle = Triangle(100, Color.RED)

    @BeforeEach
    fun setUp() {
        testPicture = Picture()
        testPicture.addShape(testSquare)
        testPicture.addShape(testCircle)
        testPicture.addLineBreak()
        testPicture.addShape(testTriangle)
    }

    @Test
    fun `getRows should return correct number of rows`() {
        val rows = Painter.getRows(testPicture)
        assertEquals(2, rows.size)
    }

    @Test
    fun `getRows should return rows with the correct properties`() {
        val (row1, row2) = Painter.getRows(testPicture)
        assertEquals(0, row1.y)
        assertEquals(200, row1.width)
        assertEquals(125, row1.height)
        assertEquals(125, row2.y)
        assertEquals(100, row2.width)
        assertEquals(100, row2.height)
    }

    @Test
    fun `getRows should return first rows with correct elements`() {
        val rows = Painter.getRows(testPicture)
        assertEquals(listOf(testSquare, testCircle), rows[0].shapes)
    }

    @Test
    fun `getRows should return second rows with correct elements`() {
        val rows = Painter.getRows(testPicture)
        assertEquals(listOf(testTriangle), rows[1].shapes)
    }
}