package ch.usi.si.codelounge.seminar.shapes.model.picture

import ch.usi.si.codelounge.seminar.shapes.model.core.LineBreak
import ch.usi.si.codelounge.seminar.shapes.model.shapes.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class PictureTest {

    lateinit var testPicture: Picture
    private val testSquare = Square(75, Color.BLUE)
    private val testCircle = Circle(125, Color.GREEN)
    private val testTriangle = Triangle(100, Color.RED)

    @BeforeEach
    fun setUp() {
        testPicture = Picture()
        testPicture.addShape(testCircle)
        testPicture.addShape(testTriangle)
        testPicture.addLineBreak()
    }

    @Test
    fun `the picture should contain the expected shapes`() {
        assertEquals(3, testPicture.getEntities().size)
        assertTrue(testPicture.getEntities().contains(testCircle))
        assertTrue(testPicture.getEntities().contains(testTriangle))
        assertTrue(testPicture.getEntities().contains(LineBreak))
    }

    @Test
    fun `addShape should add a shape last`() {
        testPicture.addShape(testSquare)
        assertTrue(testPicture.getEntities().contains(testSquare))
        assertEquals(testSquare, testPicture.getEntities().last())
    }

    @Test
    fun `addLineBreak should add the linebreak last`() {
        testPicture.addLineBreak()
        assertEquals(LineBreak, testPicture.getEntities().last())
    }
}