group = "ch.usi.si.codelounge.seminar"
version = "1.0.0"

plugins {
    kotlin("jvm" ) version "1.6.10"
    java
}

repositories {
    mavenCentral()
    mavenCentral()
    maven {
        setUrl("https://packages.jetbrains.team/maven/p/skija/maven")
    }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.1")
    api("org.jetbrains.skija:skija-macos-arm64:0.93.6")
    implementation(kotlin("script-runtime"))
}

tasks.withType<Test> {
    useJUnitPlatform()
}
